/*
  a tiny wrapper for cjs to esm compatibility
  ref : https://redfin.engineering/node-modules-at-war-why-commonjs-and-es-modules-cant-get-along-9617135eeca1
*/

import read_config from '../app.js';

export {
  read_config
};

