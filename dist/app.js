
const fs   = require('fs');
const yaml = require('js-yaml');

//################################################################
async function read_config( config_file ){

  // this is the obj storing the yarml config to be read
  let config_data_obj;

  try {
      const file_content = fs.readFileSync( config_file, 'utf8');
      config_data_obj = yaml.load( file_content );
  } catch (e) {
      console.log("error in reading yaml config : ", e);
  }

    return config_data_obj;

}//function
//################################################################
// all exports go here
module.exports = read_config;

