# dallmo-read-config

- a simple config reader based on `fs` and `js-yaml` ;
- config file assumed to be yaml ; 
- compatible with both cjs and esm, by following [the advice of a post by Dan Fabulich][ref-1]

[ref-1]: https://redfin.engineering/node-modules-at-war-why-commonjs-and-es-modules-cant-get-along-9617135eeca1

# usage

## cjs
```
const read_config = require("dallmo-read-config");
```

## esm
```
import {read_config} from 'dallmo-read-config';
```

