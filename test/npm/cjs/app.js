
const read_config = require("dallmo-read-config");

// locate the test config file
const config_file = "./sample-config.yaml";

// test read the config
read_config( config_file ).then( config_obj => {
  console.log( config_obj );
});

