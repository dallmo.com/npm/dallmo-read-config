#!/bin/bash

##############################################
function separator
{ 
  line="-------------------------------"
  echo 
  echo "$line"
  echo "$1"
  echo "$line"
}
##############################################

separator "cleaning up"
rm -vrf ./node_modules/ 

separator "re-install dependencies"
yarn install

separator "test result below"
node app.js
echo

separator "final cleaning up"
rm -rf ./node_modules/ 


